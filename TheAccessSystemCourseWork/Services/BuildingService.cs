﻿using Newtonsoft.Json;
using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Services;

public class BuildingService
    {
        private readonly string buildingsDirectory;

        public BuildingService(string buildingsDirectory)
        {
            this.buildingsDirectory = buildingsDirectory;
        }

        public List<Building> GetBuildings()
        {
            var buildings = new List<Building>();

            if (!Directory.Exists(buildingsDirectory))
            {
                return buildings;
            }

            var buildingDirectories = Directory.GetDirectories(buildingsDirectory);

            foreach (var buildingDir in buildingDirectories)
            {
                var buildingName = Path.GetFileName(buildingDir);
                var building = new Building { BuildingName = buildingName, Floors = new List<Floor>() };

                var floorDirectories = Directory.GetDirectories(buildingDir);

                foreach (var floorDir in floorDirectories)
                {
                    var floorNumberStr = Path.GetFileName(floorDir);
                    if (int.TryParse(floorNumberStr, out int floorNumber))
                    {
                        var floor = new Floor { FloorNumber = floorNumber, MapAreas = new List<MapArea>() };

                        var jsonFilePath = Path.Combine(floorDir, "mapData.json");
                        if (File.Exists(jsonFilePath))
                        {
                            var jsonData = File.ReadAllText(jsonFilePath);
                            floor.MapAreas = JsonConvert.DeserializeObject<List<MapArea>>(jsonData);
                        }

                        var images = Directory.GetFiles(floorDir, "*.*").Where(s => s.EndsWith(".jpg") || s.EndsWith(".png") || s.EndsWith(".jpeg")).ToList();
                        if (images.Any())
                        {
                            floor.FloorImage = Path.GetFileName(images.First());
                        }

                        building.Floors.Add(floor);
                    }
                }

                buildings.Add(building);
            }

            return buildings;
        }
    }