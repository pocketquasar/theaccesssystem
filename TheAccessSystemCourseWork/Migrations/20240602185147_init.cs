﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TheAccessSystemCourseWork.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FactoryAreas",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Building = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Floor = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FactoryAreas", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Passes",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsAttached = table.Column<bool>(type: "bit", nullable: false),
                    ExpiredAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Passes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "RightsLevels",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(12)", maxLength: 12, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    RequiredDepartment = table.Column<int>(type: "int", nullable: true),
                    RequiredPosition = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RightsLevels", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Checkpoints",
                columns: table => new
                {
                    ID = table.Column<string>(type: "nvarchar(12)", maxLength: 12, nullable: false),
                    AreaID = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsBlocked = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Checkpoints", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Checkpoints_FactoryAreas_AreaID",
                        column: x => x.AreaID,
                        principalTable: "FactoryAreas",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Role = table.Column<int>(type: "int", nullable: false),
                    PassID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Login = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Users_Passes_PassID",
                        column: x => x.PassID,
                        principalTable: "Passes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PassRights",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PassID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RightsLevelID = table.Column<string>(type: "nvarchar(12)", maxLength: 12, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PassRights", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PassRights_Passes_PassID",
                        column: x => x.PassID,
                        principalTable: "Passes",
                        principalColumn: "ID");
                    table.ForeignKey(
                        name: "FK_PassRights_RightsLevels_RightsLevelID",
                        column: x => x.RightsLevelID,
                        principalTable: "RightsLevels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CheckpointPassingAttempts",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PassID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CheckpointID = table.Column<string>(type: "nvarchar(12)", maxLength: 12, nullable: false),
                    Accessed = table.Column<bool>(type: "bit", nullable: false),
                    AttemptTime = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckpointPassingAttempts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CheckpointPassingAttempts_Checkpoints_CheckpointID",
                        column: x => x.CheckpointID,
                        principalTable: "Checkpoints",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CheckpointPassingAttempts_Passes_PassID",
                        column: x => x.PassID,
                        principalTable: "Passes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CheckpointRights",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CheckpointID = table.Column<string>(type: "nvarchar(12)", maxLength: 12, nullable: false),
                    RightsLevelID = table.Column<string>(type: "nvarchar(12)", maxLength: 12, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheckpointRights", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CheckpointRights_Checkpoints_CheckpointID",
                        column: x => x.CheckpointID,
                        principalTable: "Checkpoints",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CheckpointRights_RightsLevels_RightsLevelID",
                        column: x => x.RightsLevelID,
                        principalTable: "RightsLevels",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    ServiceNumber = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    SecondName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    MiddleName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    BirthDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Position = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Department = table.Column<int>(type: "int", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.ServiceNumber);
                    table.ForeignKey(
                        name: "FK_Employees_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Guests",
                columns: table => new
                {
                    Passport = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    SecondName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    MiddleName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    UserID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guests", x => x.Passport);
                    table.ForeignKey(
                        name: "FK_Guests_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PassOperations",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OperationType = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: false),
                    PassID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmployeeNumber = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PassOperations", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PassOperations_Employees_EmployeeNumber",
                        column: x => x.EmployeeNumber,
                        principalTable: "Employees",
                        principalColumn: "ServiceNumber",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PassOperations_Passes_PassID",
                        column: x => x.PassID,
                        principalTable: "Passes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CheckpointPassingAttempts_CheckpointID",
                table: "CheckpointPassingAttempts",
                column: "CheckpointID");

            migrationBuilder.CreateIndex(
                name: "IX_CheckpointPassingAttempts_PassID",
                table: "CheckpointPassingAttempts",
                column: "PassID");

            migrationBuilder.CreateIndex(
                name: "IX_CheckpointRights_CheckpointID",
                table: "CheckpointRights",
                column: "CheckpointID");

            migrationBuilder.CreateIndex(
                name: "IX_CheckpointRights_RightsLevelID",
                table: "CheckpointRights",
                column: "RightsLevelID");

            migrationBuilder.CreateIndex(
                name: "IX_Checkpoints_AreaID",
                table: "Checkpoints",
                column: "AreaID");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_ServiceNumber",
                table: "Employees",
                column: "ServiceNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employees_UserID",
                table: "Employees",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Guests_Passport",
                table: "Guests",
                column: "Passport",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Guests_UserID",
                table: "Guests",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_PassOperations_EmployeeNumber",
                table: "PassOperations",
                column: "EmployeeNumber");

            migrationBuilder.CreateIndex(
                name: "IX_PassOperations_PassID",
                table: "PassOperations",
                column: "PassID");

            migrationBuilder.CreateIndex(
                name: "IX_PassRights_PassID",
                table: "PassRights",
                column: "PassID");

            migrationBuilder.CreateIndex(
                name: "IX_PassRights_RightsLevelID",
                table: "PassRights",
                column: "RightsLevelID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Login",
                table: "Users",
                column: "Login",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_PassID",
                table: "Users",
                column: "PassID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CheckpointPassingAttempts");

            migrationBuilder.DropTable(
                name: "CheckpointRights");

            migrationBuilder.DropTable(
                name: "Guests");

            migrationBuilder.DropTable(
                name: "PassOperations");

            migrationBuilder.DropTable(
                name: "PassRights");

            migrationBuilder.DropTable(
                name: "Checkpoints");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "RightsLevels");

            migrationBuilder.DropTable(
                name: "FactoryAreas");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Passes");
        }
    }
}
