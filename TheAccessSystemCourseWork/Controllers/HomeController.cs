﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Controllers;

[ApiController]
[Route("{controller}")]
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }


    [Route("Index")]
    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }

    [Route("Privacy")]
    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}