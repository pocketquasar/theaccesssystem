﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Controllers;

[ApiController]
[Route("{controller}")]
public class PassController : Controller
{
    private readonly IPassRepository _passRepository;
    private readonly IPassRightsRepository _passRightsRepository;
    private readonly IRightsLevelRepository _rightsLevelRepository;

    public PassController(IPassRepository passRepository,
                        IPassRightsRepository passRightsRepository,
                        IRightsLevelRepository rightsLevelRepository)
    {
        _passRepository = passRepository;
        _passRightsRepository = passRightsRepository;
        _rightsLevelRepository = rightsLevelRepository;
    }

    [HttpGet("Details/{id}")]
    public async Task<IActionResult> Details(Guid id)
    {
        try
        {
            var pass = await _passRepository.GetPassByIdAsync(id);
            var rightsLevels = await _rightsLevelRepository.FindRightsLevelsAsync();

            ViewBag.RightsLevels = rightsLevels;
            return View(pass);
        }
        catch (Exception ex)
        {
            return View("Error");
        }
    }

    [HttpPost("UpdatePass/{request}")]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> UpdatePass([FromForm] UpdatePassRequest request)
    {
        try
        {
            var existingPass = await _passRepository.GetPassByIdAsync(request.Pass.ID);
            if (existingPass == null)
            {
                return NotFound("Pass not found.");
            }
    
            existingPass.IsAttached = request.Pass.IsAttached;
            existingPass.IsActive = request.Pass.IsActive;
            existingPass.ExpiredAt = request.Pass.ExpiredAt;
    
            existingPass.PassRights.Clear();
            foreach (var rightsId in request.RightsIds)
            {
                existingPass.PassRights.Add(new PassRights
                {
                    PassID = request.Pass.ID,
                    RightsLevelID = rightsId
                });
            }
    
            await _passRepository.UpdatePassAsync(existingPass);
            return RedirectToAction("Login", "User");
        }
        catch (Exception ex)
        {
            return View("Error");
        }
    }
    
    public class UpdatePassRequest
    {
        public Pass Pass { get; set; }
        public List<string> RightsIds { get; set; }
    }

}