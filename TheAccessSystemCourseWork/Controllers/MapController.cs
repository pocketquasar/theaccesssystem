﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;
using TheAccessSystemCourseWork.Services;

namespace TheAccessSystemCourseWork.Controllers;

[ApiController]
[Route("Map")]
public class MapController : Controller
{
    private readonly BuildingService _buildingService;
    private readonly IFactoryAreaRepository _factoryAreaRepository;
    private readonly ICheckpointRepository _checkpointRepository;
    private readonly IUserRepository _userRepository;
    public MapController(BuildingService buildingService, 
                        IFactoryAreaRepository factoryAreaRepository,
                        IUserRepository userRepository,
                        ICheckpointRepository checkpointRepository)
    {
        _buildingService = buildingService;
        _factoryAreaRepository = factoryAreaRepository;
        _checkpointRepository = checkpointRepository;
        _userRepository = userRepository;
    }

    [HttpGet("UploadMap")]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> UploadMap()
    {
        var factoryAreas = await _factoryAreaRepository.FindFactoryAreasAsync();
        ViewBag.Buildings = factoryAreas.Select(fa => fa.Building).Distinct().ToList();

        var floors = new Dictionary<string, List<int>>();
        foreach (var area in factoryAreas)
        {
            if (!floors.ContainsKey(area.Building))
            {
                floors[area.Building] = new List<int>();
            }
            if (!floors[area.Building].Contains(area.Floor))
            {
                floors[area.Building].Add(area.Floor);
            }
        }

        ViewBag.Floors = floors;
        return View(_buildingService.GetBuildings());
    }

    [HttpPost("UploadMap")]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> UploadMap([FromForm]IFormFile mapImage, [FromForm]string buildingName, [FromForm]int floorNumber)
{
    if (mapImage != null && mapImage.Length > 0)
    {
        var fileName = Path.GetFileName(mapImage.FileName);
        var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", buildingName, floorNumber.ToString(), fileName);

        Directory.CreateDirectory(Path.GetDirectoryName(filePath));

        using (var stream = new FileStream(filePath, FileMode.Create))
        {
            await mapImage.CopyToAsync(stream);
        }

        return RedirectToAction("EditMap", new { buildingName, floorNumber });
    }

    var factoryAreas = await _factoryAreaRepository.FindFactoryAreasAsync();
    ViewBag.Buildings = factoryAreas.Select(fa => fa.Building).Distinct().ToList();

    var floors = new Dictionary<string, List<int>>();
    foreach (var area in factoryAreas)
    {
        if (!floors.ContainsKey(area.Building))
        {
            floors[area.Building] = new List<int>();
        }
        if (!floors[area.Building].Contains(area.Floor))
        {
            floors[area.Building].Add(area.Floor);
        }
    }

    ViewBag.Floors = floors;
    return View("UploadMap", _buildingService.GetBuildings());
}

    [HttpGet("EditMap")]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> EditMap(string buildingName, int floorNumber)
    {
        var buildings = _buildingService.GetBuildings();
        var building = buildings.FirstOrDefault(b => b.BuildingName == buildingName);
        var floor = building?.Floors.FirstOrDefault(f => f.FloorNumber == floorNumber);

        ViewBag.BuildingName = buildingName;
        ViewBag.FloorNumber = floorNumber;
        ViewBag.FloorImagePath = floor != null ? $"/images/{buildingName}/{floorNumber}/{floor.FloorImage}" : null;
        ViewBag.MapAreas = floor?.MapAreas ?? new List<MapArea>();

        var factoryAreas = await _factoryAreaRepository.FindFactoryAreasAsync(buildingName, floorNumber);
        ViewBag.FactoryAreas = factoryAreas;

        return View();
    }


    [HttpPost("SaveMapData")]
    [Authorize(Roles = "Admin")]
    public IActionResult SaveMapData([FromForm]string buildingName, [FromForm]int floorNumber, [FromForm]string mapData)
    {
        try
        {
            var directoryPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", buildingName, floorNumber.ToString());
            var filePath = Path.Combine(directoryPath, "mapData.json");

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
            var mapDataList = JsonConvert.DeserializeObject<List<MapArea>>(mapData);
            System.IO.File.WriteAllText(filePath, JsonConvert.SerializeObject(mapDataList));
        }
        catch (ArgumentNullException ex)
        {
            return RedirectToAction("UploadMap");
        }
        return RedirectToAction("UploadMap");
    }
    
    [HttpGet("ViewBuildings")]
    public async Task<IActionResult> ViewBuildings()
    {
        var buildings = _buildingService.GetBuildings();

        var areas = await _factoryAreaRepository.FindFactoryAreasAsync();
        var checkpoints = await _checkpointRepository.FindCheckpointsAsync();
        var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

        if (userId == null)
        {
            return Unauthorized();
        }

        var userIdGuid = Guid.Parse(userId);
        var currentUser = await _userRepository.GetUserByIdAsync(userIdGuid);
        var currentUserRights = currentUser?.Pass.PassRights.ToList().Select(pr => pr.RightsLevel.ID).ToList() ?? new List<string>();

        var areasRights = new Dictionary<string, bool>(); 

        foreach (var area in areas)
        {
            Console.WriteLine(area.ID);
            var areaRights = checkpoints?
                .Where(ch => ch.AreaID == area.ID).FirstOrDefault()?
                .CheckpointRights
                .Select(ch => ch.RightsLevelID)
                .ToList();

            var hasCommonRights = areaRights?.Any(ar => currentUserRights.Contains(ar));
            
            Console.WriteLine($"{area.Name}: {hasCommonRights}");
            
            if (hasCommonRights.HasValue)
                areasRights.Add(area.Name, hasCommonRights.Value);
        }

        
        foreach (var kvp in areasRights)
        {
            Console.WriteLine($"Area: {kvp.Key}, HasRights: {kvp.Value}");
        }

        ViewBag.AreasRights = areasRights;

        return View(buildings);
    }


    [HttpGet]
    public async Task<IActionResult> GetMapData(string buildingName, int floorNumber)
    {
        var buildings = _buildingService.GetBuildings();
        var building = buildings.FirstOrDefault(b => b.BuildingName == buildingName);
        var floor = building?.Floors.FirstOrDefault(f => f.FloorNumber == floorNumber);

        if (floor == null)
        {
            return NotFound();
        }

        var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", buildingName, floorNumber.ToString(), "mapData.json");
        if (!System.IO.File.Exists(filePath))
        {
            return NotFound();
        }

        var mapData = System.IO.File.ReadAllText(filePath);
        var mapAreas = JsonConvert.DeserializeObject<List<MapArea>>(mapData);

        return Ok(mapAreas);
    }

    // [HttpGet("ViewBuildings")]
    // public IActionResult ViewBuildings()
    // {
    //     var buildings = _buildingService.GetBuildings();
    //     return View(buildings);
    // }
    //
    // [HttpGet]
    // public IActionResult GetMapData(string buildingName, int floorNumber)
    // {
    //     var buildings = _buildingService.GetBuildings();
    //     var building = buildings.FirstOrDefault(b => b.BuildingName == buildingName);
    //     var floor = building?.Floors.FirstOrDefault(f => f.FloorNumber == floorNumber);
    //
    //     if (floor == null)
    //     {
    //         return NotFound();
    //     }
    //
    //     var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", buildingName, floorNumber.ToString(), "mapData.json");
    //     if (!System.IO.File.Exists(filePath))
    //     {
    //         return NotFound();
    //     }
    //
    //     var mapData = System.IO.File.ReadAllText(filePath);
    //     var mapAreas = JsonConvert.DeserializeObject<List<MapArea>>(mapData);
    //
    //     return Ok(mapAreas);
    // }
}