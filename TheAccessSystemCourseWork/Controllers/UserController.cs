﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;
using TheAccessSystemCourseWork.Services;

namespace TheAccessSystemCourseWork.Controllers;

[ApiController]
[Route("{controller}")]
public class UserController : Controller
{
    private readonly IUserRepository _userRepository;
    private readonly IPassRepository _passRepository;
    private readonly IGuestRepository _guestRepository;
    private readonly IEmployeeRepository _employeeRepository;
    private readonly AuthService _authService;

    public UserController(IUserRepository userRepository, 
                          IPassRepository passRepository,
                          IGuestRepository guestRepository,
                          IEmployeeRepository employeeRepository,
                          AuthService authService)
    {
        _userRepository = userRepository;
        _passRepository = passRepository;
        _guestRepository = guestRepository;
        _employeeRepository = employeeRepository;
        _authService = authService;
    }
    
    [Route("Login")]
    [HttpGet]
    public IActionResult Login()
    {
        return View();
    }
    
    [Route("Login")]
    [HttpPost]
    public async Task<IActionResult> Login([FromForm]string login, [FromForm]string password)
    {
        if (!await _authService.ValidateUserCredentials(login, password))
        {
            ModelState.AddModelError(string.Empty, "Invalid login or password.");
            return View();
        }

        var user = await _userRepository.GetUserByLoginAsync(login);

        await _authService.SignInUser(HttpContext, user);

        return RedirectToAction("PersonalAccount");
    }
    
    [Route("PersonalAccount")]
    [HttpGet]
    public async Task<IActionResult> PersonalAccount()
    {
        var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        if (userId == null)
        {
            return Unauthorized();
        }

        var user = await _userRepository.GetUserByIdAsync(Guid.Parse(userId));
        if (user == null)
        {
            return NotFound("User not found.");
        }

        if (User.IsInRole("Guest"))
        {
            var guest = await _guestRepository.GetGuestByUserIDAsync(user.ID);
            if (guest == null)
            {
                return NotFound("Информация о госте не найдена.");
            }
            var viewModel = new PersonalAccountViewModel
            {
                User = user,
                Guest = guest
            };
            return View(viewModel);
        }
        else
        {
            var employee = await _employeeRepository.GetEmployeeByUserIDAsync(user.ID);
            if (employee == null)
            {
                return NotFound("Информации о сотруднике не найдено.");
            }
            var viewModel = new PersonalAccountViewModel
            {
                User = user,
                Employee = employee
            };
            return View(viewModel);
        }
    }

    [Route("PersonalAccount/UpdatePassword")]
    [HttpGet]
    public IActionResult UpdatePassword()
    {
        return View();
    }
    
    [Route("PersonalAccount/UpdatePassword")]
    [HttpPost]
    public async Task<IActionResult> UpdatePassword([FromForm]string newPassword)
    {
        var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

        if (userId == null)
        {
            return Unauthorized();
        }

        if (string.IsNullOrWhiteSpace(newPassword))
        {
            ModelState.AddModelError(string.Empty, "Поле не должно быть пустым.");
            return View();
        }

        var userIdGuid = Guid.Parse(userId);
        var success = await _userRepository.UpdatePasswordAsync(userIdGuid, _authService.HashPassword(newPassword));

        if (!success)
        {
            ModelState.AddModelError(string.Empty, "Не удалось изменить пароль.");
            return View();
        }

        return RedirectToAction("PersonalAccount"); 
    }

    
    [Route("Logout")]
    [HttpGet]
    public async Task<IActionResult> Logout()
    {
        await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        return RedirectToAction("Login");
    }
    
    [Authorize(Roles = "Admin, PassOfficeEmployee")]
    [HttpGet("Details/{id}")]
    public async Task<IActionResult> Details(Guid id)
    {
        var user = await _userRepository.GetUserByIdAsync(id);
        if (user == null)
        {
            return NotFound();
        }
        return View(user);
    }
    
    [HttpPost("Update/{id}")]
    [Authorize(Roles = "Admin, PassOfficeEmployee")]
    public async Task<IActionResult> Update(Guid id, [FromForm] User user)
    {
        if (id != user.ID)
        {
            return BadRequest("Несоответствие идентификатора пользователя.");
        }

        if (!ModelState.IsValid)
        {
            return View("Details", user);
        }

        var existingUser = await _userRepository.GetUserByIdAsync(id);
        if (existingUser == null)
        {
            return NotFound();
        }

        existingUser.Role = user.Role;
        existingUser.Login = user.Login;
        existingUser.Password = user.Password;

        var updatedUser = await _userRepository.UpdateUserAsync(existingUser);
        if (updatedUser == null)
        {
            return StatusCode(500, "An error occurred while updating the user.");
        }

        return RedirectToAction("Details", new { id = user.ID });
    }

}
