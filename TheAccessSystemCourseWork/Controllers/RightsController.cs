﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Controllers
{
    [ApiController]
    [Route("[controller]")]
    //[Authorize(Roles = "Admin")]
    public class RightsController : Controller
    {
        private readonly IRightsLevelRepository _rightsLevelRepository;

        public RightsController(IRightsLevelRepository rightsLevelRepository)
        {
            _rightsLevelRepository = rightsLevelRepository;
        }

        [HttpGet("List")]
        public async Task<IActionResult> List()
        {
            try
            {
                var rightsList = await _rightsLevelRepository.FindRightsLevelsAsync();
                return View(rightsList);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost("Create")]
        public async Task<IActionResult> CreateRights([FromForm] RightsLevel rightsLevel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var success = await _rightsLevelRepository.AddRightsLevelAsync(rightsLevel);
                if (success)
                {
                    return RedirectToAction("List");
                }

                return StatusCode(500, "An error occurred while creating the rights level.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }


        [HttpGet("Update/{id}")]
        public async Task<IActionResult> Update(string id)
        {
            try
            {
                var rightsLevel = await _rightsLevelRepository.GetRightsLevelByIdAsync(id);
                if (rightsLevel == null)
                {
                    return NotFound($"Права с идентификатором {id} не найдены.");
                }

                return View(rightsLevel);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpPost("Update")]
        public async Task<IActionResult> UpdateRights([FromForm] RightsLevel rightsLevel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var success = await _rightsLevelRepository.UpdateRightsLevelAsync(rightsLevel);
                if (success)
                {
                    return RedirectToAction("List");
                }

                return NotFound($"Права с идентификатором {rightsLevel.ID} не найдены.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
    }
}
