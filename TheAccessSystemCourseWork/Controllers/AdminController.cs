﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Extensions;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize(Roles = "Admin")]
public class AdminController : Controller
{
    private readonly IEmployeeRepository _employeeRepository;
    private readonly IUserRepository _userRepository;
    private readonly IRightsLevelRepository _rightsLevelRepository;
    private readonly ICheckpointRepository _checkpointRepository;
    private readonly ICheckpointRightsRepository _checkpointRightsRepository;
    private readonly IPassRepository _passRepository;
    private readonly IPassRightsRepository _passRightsRepository;
    private readonly IFactoryAreaRepository _factoryAreaRepository;

    public AdminController(IEmployeeRepository employeeRepository,
        IUserRepository userRepository,
        IRightsLevelRepository rightsLevelRepository,
        ICheckpointRepository checkpointRepository,
        ICheckpointRightsRepository checkpointRightsRepository,
        IPassRepository passRepository,
        IPassRightsRepository passRightsRepository,
        IFactoryAreaRepository factoryAreaRepository)
    {
        _employeeRepository = employeeRepository;
        _userRepository = userRepository;
        _rightsLevelRepository = rightsLevelRepository;
        _checkpointRepository = checkpointRepository;
        _checkpointRightsRepository = checkpointRightsRepository;
        _passRepository = passRepository;
        _passRightsRepository = passRightsRepository;
        _factoryAreaRepository = factoryAreaRepository;
    }

    [HttpGet("Checkpoints")] // done заменить удаление, изменение и блокировку на картинки
    public async Task<IActionResult> Checkpoints()
    {
        try
        {
            var checkpoints = await _checkpointRepository.FindCheckpointsAsync();
            return View(checkpoints);
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }

    [HttpGet("AddCheckpoint")] // done
    public async Task<IActionResult> AddCheckpoint()
    {
        var rightsLevels = await _rightsLevelRepository.FindRightsLevelsAsync();
        var factoryAreas = await _factoryAreaRepository.FindFactoryAreasAsync();
        ViewBag.RightsLevels = rightsLevels;
        ViewBag.FactoryAreas = factoryAreas;
        return View();
    }

    [HttpPost("AddCheckpoint")] // done изменить только представление зон
    public async Task<IActionResult> AddCheckpoint([FromForm] Checkpoint checkpoint, [FromForm] List<string> rightsIds)
    {
        if (ModelState.IsValid)
        {
            foreach (var rightsId in rightsIds)
            {
                var rightsLevel = await _rightsLevelRepository.GetRightsLevelByIdAsync(rightsId);
                if (rightsLevel != null)
                {
                    checkpoint.CheckpointRights.Add(new CheckpointRights { RightsLevel = rightsLevel });
                }
            }

            await _checkpointRepository.AddCheckpointAsync(checkpoint);
            return RedirectToAction("Checkpoints");
        }

        var rightsLevels = await _rightsLevelRepository.FindRightsLevelsAsync();
        ViewBag.RightsLevels = rightsLevels;
        return View(checkpoint);
    }
    
    [HttpGet("UpdateCheckpoint/{checkpointID}")] // done
    public async Task<IActionResult> UpdateCheckpoint(string checkpointID)
    {
        try
        {
            var checkpoint = await _checkpointRepository.GetCheckpointByIdAsync(checkpointID);
            var rightsLevels = await _rightsLevelRepository.FindRightsLevelsAsync();
            var factoryAreas = await _factoryAreaRepository.FindFactoryAreasAsync();

            ViewBag.RightsLevels = rightsLevels;
            ViewBag.FactoryAreas = factoryAreas;
            return View(checkpoint);
        }
        catch (Exception ex)
        {
            return View("Error");
        }
    }
    
    [HttpPost("UpdateCheckpoint/{request}")] // готово, мб только checkboxы изменить
    public async Task<IActionResult> UpdateCheckpoint([FromBody] UpdateCheckpointRequest request)
    {
        try
        {
            var existingCheckpoint = await _checkpointRepository.GetCheckpointByIdAsync(request.Checkpoint.ID);
            if (existingCheckpoint == null)
            {
                return NotFound("Checkpoint not found.");
            }
    
            existingCheckpoint.AreaID = request.Checkpoint.AreaID;
            existingCheckpoint.IsActive = request.Checkpoint.IsActive;
            existingCheckpoint.IsBlocked = request.Checkpoint.IsBlocked;
    
            existingCheckpoint.CheckpointRights.Clear();
            foreach (var rightsId in request.RightsIds)
            {
                existingCheckpoint.CheckpointRights.Add(new CheckpointRights
                {
                    CheckpointID = request.Checkpoint.ID,
                    RightsLevelID = rightsId
                });
            }
    
            await _checkpointRepository.UpdateCheckpointAsync(existingCheckpoint);
            return RedirectToAction("Checkpoints");
        }
        catch (Exception ex)
        {
            return View("Error");
        }
    }
    
    public class UpdateCheckpointRequest
    {
        public Checkpoint Checkpoint { get; set; }
        public List<string> RightsIds { get; set; }
    }
    
    [HttpDelete("DeleteCheckpoint/{checkpointId}")] // готово
    public async Task<IActionResult> DeleteCheckpoint(string checkpointId)
    {
        try
        {
            await _checkpointRepository.DeleteCheckpointAsync(checkpointId);
            return Ok("Checkpoint deleted successfully.");
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }

    [HttpPost("BlockCheckpoint")] // done
    public async Task<IActionResult> BlockCheckpoint([FromForm] string checkpointId)
    {
        try
        {
            var checkpoint = await _checkpointRepository.GetCheckpointByIdAsync(checkpointId);
            if (checkpoint == null)
            {
                return NotFound($"Checkpoint with ID {checkpointId} not found.");
            }

            checkpoint.IsBlocked = true;
            await _checkpointRepository.UpdateCheckpointAsync(checkpoint);

            return RedirectToAction("Checkpoints");
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }
    
    [HttpPost("UnlockCheckpoint")] // done
    public async Task<IActionResult> UnlockCheckpoint([FromForm] string checkpointId)
    {
        try
        {
            var checkpoint = await _checkpointRepository.GetCheckpointByIdAsync(checkpointId);
            if (checkpoint == null)
            {
                return NotFound($"Checkpoint with ID {checkpointId} not found.");
            }

            checkpoint.IsBlocked = false;
            await _checkpointRepository.UpdateCheckpointAsync(checkpoint);

            return RedirectToAction("Checkpoints");
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }

    [HttpPost("UnlockCheckpoints")] // done
    public async Task<IActionResult> UnlockCheckpoints()
    {
        try
        {
            var checkpoints = await _checkpointRepository.FindCheckpointsAsync();
            foreach (var checkpoint in checkpoints)
            {
                checkpoint.IsActive = false;
            }

            await _checkpointRepository.UpdateCheckpointsAsync(checkpoints);

            return Ok("All checkpoints are unlocked.");
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }

    [HttpPost("ChangePassRights")]
    public async Task<IActionResult> ChangePassRights(Guid passId, List<string> rightsIds)
    {
        try
        {
            var pass = await _passRepository.GetPassByIdAsync(passId);
            if (pass == null)
            {
                return NotFound($"Pass with ID {passId} not found.");
            }

            pass.PassRights.Clear();

            foreach (var rightId in rightsIds)
            {
                var rightsLevel = await _rightsLevelRepository.GetRightsLevelByIdAsync(rightId);
                if (rightsLevel != null)
                {
                    pass.PassRights.Add(new PassRights { RightsLevel = rightsLevel });
                }
            }

            await _passRepository.UpdatePassAsync(pass);

            return Ok($"Pass with ID {passId} rights updated successfully.");
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }
    
    [HttpGet("FactoryAreas")]
    public async Task<IActionResult> FactoryAreas()
    {
        try
        {
            var factoryAreas = await _factoryAreaRepository.FindFactoryAreasAsync();
            return View(factoryAreas);
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }

    [HttpGet("AddFactoryArea")]
    public IActionResult AddFactoryArea()
    {
        return View();
    }

    [HttpPost("AddFactoryArea")]
    public async Task<IActionResult> AddFactoryArea([FromForm] FactoryArea factoryArea)
    {
        if (ModelState.IsValid)
        {
            await _factoryAreaRepository.AddFactoryAreaAsync(factoryArea);
            return RedirectToAction("FactoryAreas");
        }
        return View(factoryArea);
    }

    [HttpGet("UpdateFactoryArea/{id}")]
    public async Task<IActionResult> UpdateFactoryArea(int id)
    {
        var factoryArea = await _factoryAreaRepository.GetFactoryAreaByIdAsync(id);
        if (factoryArea == null)
        {
            return NotFound();
        }
        return View(factoryArea);
    }

    [HttpPost("UpdateFactoryArea/{id}")]
    public async Task<IActionResult> UpdateFactoryArea(int id, [FromForm] FactoryArea factoryArea)
    {
        if (id != factoryArea.ID)
        {
            return BadRequest("FactoryArea ID mismatch.");
        }

        if (ModelState.IsValid)
        {
            var updatedFactoryArea = await _factoryAreaRepository.UpdateFactoryAreaAsync(factoryArea);
            if (updatedFactoryArea == null)
            {
                return StatusCode(500, "An error occurred while updating the factory area.");
            }
            return RedirectToAction("FactoryAreas");
        }
        return View(factoryArea);
    }

    [HttpPost("DeleteFactoryArea/{id}")]
    public async Task<IActionResult> DeleteFactoryArea(int id)
    {
        try
        {
            await _factoryAreaRepository.DeleteFactoryAreaAsync(id);
            return RedirectToAction("FactoryAreas");
        }
        catch (Exception ex)
        {
            return StatusCode(500, $"Internal server error: {ex.Message}");
        }
    }

}
