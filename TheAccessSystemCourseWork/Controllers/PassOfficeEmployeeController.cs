﻿using System.Runtime.InteropServices.JavaScript;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;
using TheAccessSystemCourseWork.Services;

namespace TheAccessSystemCourseWork.Controllers
{
    [ApiController]
    [Route("PassOfficeEmployee")]
    [Authorize(Roles = "PassOfficeEmployee, Admin")]
    public class PassOfficeEmployeeController : Controller
    {
        private readonly IPassRepository _passRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPassRightsRepository _passRightsRepository;
        private readonly IUserRepository _userRepository;
        private readonly IPassOperationRepository _passOperationRepository;
        private readonly IRightsLevelRepository _rightsLevelRepository;
        private readonly IGuestRepository _guestRepository;
        private readonly AuthService _authService;

        public PassOfficeEmployeeController(IPassRepository passRepository,
                                            IEmployeeRepository employeeRepository,
                                            IPassRightsRepository passRightsRepository,
                                            IUserRepository userRepository,
                                            IPassOperationRepository passOperationRepository,
                                            IRightsLevelRepository rightsLevelRepository,
                                            IGuestRepository guestRepository,
                                            AuthService authService)
        {
            _passRepository = passRepository;
            _employeeRepository = employeeRepository;
            _passRightsRepository = passRightsRepository;
            _userRepository = userRepository;
            _passOperationRepository = passOperationRepository;
            _rightsLevelRepository = rightsLevelRepository;
            _guestRepository = guestRepository;
            _authService = authService;
        }

        [HttpGet("ViewEmployees")]
        public async Task<IActionResult> ViewEmployees(  
            int? serviceNumber = null,
            string? firstName = null,
            string? secondName = null,
            string? position = null,
            int? department = null,
            DateTime? birthDate = null)
        {
            try
            {
                var employees = await _employeeRepository.FindEmployeesAsync(
                    serviceNumber, 
                    firstName,
                    secondName,
                    position,
                    department,
                    birthDate);
                
                return View(employees);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ошибка на сервере: {ex.Message}");
            }
        }
        
        [HttpGet("ViewGuests")]
        public async Task<IActionResult> ViewGuests(
            string? firstName = null,
            string? secondName = null,
            string? middleName = null,
            Int64? passport = null,
            string? phoneNumber = null)
        {
            var guests = await _guestRepository.FindGuestsAsync(firstName, secondName, middleName, passport, phoneNumber);
            return View(guests);
        }

        [HttpGet("CreateGuest")]
        public async Task<IActionResult> CreateGuest()
        {
            var users = await _userRepository.FindUsersAsync(Roles.Guest);
            var linkedUserIds = (await _employeeRepository.FindEmployeesAsync()).Select(e => e.UserID)
                .Concat((await _guestRepository.FindGuestsAsync()).Select(g => g.UserID)).ToList();
            var availableUsers = users.Where(u => !linkedUserIds.Contains(u.ID)).ToList();

            ViewBag.AvailableUsers = availableUsers;

            return View();
        }
        
        [HttpGet("EditGuest/{passport}")]
        public async Task<IActionResult> EditGuest(Int64 passport)
        {
            var guest = await _guestRepository.GetGuestByPassport(passport);
            if (guest == null)
            {
                return NotFound();
            }

            return View(guest);
        }

        [HttpPost("EditGuest/{passport}")]
        [Authorize(Roles = "Admin, PassOfficeEmployee")]
        public async Task<IActionResult> EditGuest(Int64 passport, [FromForm] Guest guest)
        {
            if (passport != guest.Passport)
            {
                return BadRequest("The passport number does not match the guest's passport number.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var existingGuest = await _guestRepository.GetGuestByPassport(passport);
                    if (existingGuest == null)
                    {
                        return NotFound();
                    }

                    existingGuest.FirstName = guest.FirstName;
                    existingGuest.SecondName = guest.SecondName;
                    existingGuest.MiddleName = guest.MiddleName;
                    existingGuest.PhoneNumber = guest.PhoneNumber;
                    existingGuest.UserID = guest.UserID;

                    var success = await _guestRepository.UpdateGuestAsync(existingGuest);
                    if (success)
                    {
                        return RedirectToAction("ViewGuests");
                    }

                    return StatusCode(500, "An error occurred while updating the guest.");
                }
                catch (Exception ex)
                {
                    return StatusCode(500, $"Server error: {ex.Message}");
                }
            }
    
            return View(guest);
        }

        [HttpPost("CreateGuest")]
        public async Task<IActionResult> CreateGuest([FromForm]Guest guest)
        {
            if (ModelState.IsValid)
            {
                await _guestRepository.CreateGuestAsync(guest);
                return RedirectToAction("ViewGuests");
            }

            return RedirectToAction("CreateGuest");
        }

        [HttpPost("EditEmployee")]
        [Authorize(Roles = "Admin, PassOfficeEmployee")]
        public async Task<IActionResult> UpdateEmployee([FromForm] Employee updatedEmployee)
        {
            try
            {
                var existingEmployee = await _employeeRepository.GetEmployeeByServiceNumber(updatedEmployee.ServiceNumber);
                if (existingEmployee == null)
                {
                    return NotFound();
                }

                existingEmployee.FirstName = updatedEmployee.FirstName;
                existingEmployee.SecondName = updatedEmployee.SecondName;
                existingEmployee.MiddleName = updatedEmployee.MiddleName;
                existingEmployee.Department = updatedEmployee.Department;
                existingEmployee.Position = updatedEmployee.Position;
                existingEmployee.BirthDate = updatedEmployee.BirthDate;
                existingEmployee.PhoneNumber = updatedEmployee.PhoneNumber;

                var success = await _employeeRepository.UpdateEmployeeAsync(existingEmployee);
                if (success)
                {
                    return RedirectToAction("ViewEmployees");
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ошибка на сервере: {ex.Message}");
            }
        }
        
        [HttpGet("EditEmployee/{serviceNumber}")]
        public async Task<IActionResult> EditEmployee(int serviceNumber)
        {
            try
            {
                var employee = await _employeeRepository.GetEmployeeByServiceNumber(serviceNumber);
                if (employee == null)
                {
                    return NotFound();
                }

                return View("EditEmployee", employee);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ошибка на сервере: {ex.Message}");
            }
        }
        
        [HttpGet("CreateEmployee")]
        public async Task<IActionResult> CreateEmployee()
        {
            var users = await _userRepository.FindUsersAsync();
            var linkedUserIds = (await _employeeRepository.FindEmployeesAsync()).Select(e => e.UserID)
                .Concat((await _guestRepository.FindGuestsAsync()).Select(g => g.UserID)).ToList();
            var availableUsers = users.Where(u => !linkedUserIds.Contains(u.ID)).ToList();
        
            ViewBag.AvailableUsers = availableUsers;
        
            return View();
        }

        [HttpPost("CreateEmployee")]
        public async Task<IActionResult> CreateEmployee([FromForm]Employee employee)
        {
            if (ModelState.IsValid)
            {
                await _employeeRepository.CreateEmployeeAsync(employee);
                return RedirectToAction("ViewEmployees");
            }

            return RedirectToAction("CreateEmployee");
        }

        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUser([FromForm]User newUser)
        {
            try
            {
                var hashedPassword = _authService.HashPassword(newUser.Password);
                newUser.Password = hashedPassword;
                var success = await _userRepository.CreateUserAsync(newUser);
                if (success)
                {
                    return newUser.Role == Roles.Guest ? RedirectToAction("CreateGuest") : RedirectToAction("CreateEmployee");
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ошибка на сервере: {ex.Message}");
            }
        }

        [HttpGet("CreateUser")]
        public async Task<IActionResult> CreateUser()
        {
            try
            {
                var passes = await _passRepository.FindPassesAsync(false); 
                ViewBag.PassList = new SelectList(passes, "ID", "Type");
                return View();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpGet("CreatePass")]
        public async Task<IActionResult> CreatePass()
        {
            var rights = await _rightsLevelRepository.FindRightsLevelsAsync();
            ViewBag.RightsLevels = rights;
            return View();
        }

        [HttpPost("CreatePass")]
        public async Task<IActionResult> CreatePass([FromForm] Pass newPass, [FromForm] List<string> rightsIds)
        {
            if (ModelState.IsValid)
            {
                newPass.PassRights = new List<PassRights>();

                foreach (var rightsId in rightsIds)
                {
                    var rightsLevel = await _rightsLevelRepository.GetRightsLevelByIdAsync(rightsId);
                    if (rightsLevel != null)
                    {
                        newPass.PassRights.Add(new PassRights { RightsLevel = rightsLevel });
                    }
                }

                var success = await _passRepository.CreatePassAsync(newPass);
                if (success)
                {
                    return RedirectToAction("ViewPasses");
                }

                ModelState.AddModelError(string.Empty, "Failed to create pass.");
            }

            var rightsLevels = await _rightsLevelRepository.FindRightsLevelsAsync();
            ViewBag.RightsLevels = rightsLevels;
            return View(newPass);
        }

        [HttpPost("LockPass")]
        public async Task<IActionResult> LockPass([FromForm]Guid passId)
        {
            try
            {
                var pass = await _passRepository.GetPassByIdAsync(passId);

                pass.IsActive = false;
                
                var success = await _passRepository.UpdatePassAsync(pass);
                if (success)
                {
                    return RedirectToAction("ViewPasses");
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ошибка на сервере: {ex.Message}");
            }
        }

        [HttpPost("UnlockPass")]
        public async Task<IActionResult> UnlockPass([FromForm]Guid passId)
        {
            try
            {

                var pass = await _passRepository.GetPassByIdAsync(passId);

                pass.IsActive = true;
                
                var success = await _passRepository.UpdatePassAsync(pass);
                if (success)
                {
                    return RedirectToAction("ViewPasses");
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ошибка на сервере: {ex.Message}");
            }
        }

        [HttpGet("ViewPasses")]
        public async Task<IActionResult> ViewPasses()
        {
            try
            {
                var passes = await _passRepository.FindPassesAsync();
                return View(passes);
            }
            catch (Exception ex)
            {
                return StatusCode(400, $"Ошибка на сервере: {ex.Message}");
            }
        }
    }
}
