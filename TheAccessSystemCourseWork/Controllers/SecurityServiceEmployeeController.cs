﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TheAccessSystemCourseWork.Repositories.Interfaces;
namespace TheAccessSystemCourseWork.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(Roles = "SecurityServiceEmployee, Admin")]
    public class SecurityServiceEmployeeController : Controller
    {
        private readonly ICheckpointPassingAttemptsRepository _checkpointPassingAttemptsRepository;
        private readonly ICheckpointRepository _checkpointRepository;

        public SecurityServiceEmployeeController(
            ICheckpointPassingAttemptsRepository checkpointPassingAttemptsRepository,
            ICheckpointRepository checkpointRepository)
        {
            _checkpointPassingAttemptsRepository = checkpointPassingAttemptsRepository;
            _checkpointRepository = checkpointRepository;
        }

        [HttpGet("PassingAttempts")]
        public async Task<IActionResult> GetPassingAttempts(DateTime date)
        {
            try
            {
                var passingAttempts = await _checkpointPassingAttemptsRepository.FindCheckpointPassingAttemptsAsync(
                    attemptTime: date.Date);
                return PartialView("AttemptsPartial", passingAttempts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpPost("BlockCheckpoint")]
        public async Task<IActionResult> BlockCheckpoint(string checkpointId)
        {
            try
            {
                var checkpoint = await _checkpointRepository.GetCheckpointByIdAsync(checkpointId);
                if (checkpoint == null)
                {
                    return NotFound($"Checkpoint with ID {checkpointId} not found.");
                }

                checkpoint.IsBlocked = true;
                await _checkpointRepository.UpdateCheckpointAsync(checkpoint);

                return Ok($"Checkpoint with ID {checkpointId} is blocked.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
        
        
        [HttpPost("UnblockCheckpoint")]
        public async Task<IActionResult> UnblockCheckpoint(string checkpointId)
        {
            try
            {
                var checkpoint = await _checkpointRepository.GetCheckpointByIdAsync(checkpointId);
                if (checkpoint == null)
                {
                    return NotFound($"Checkpoint with ID {checkpointId} not found.");
                }
                if (checkpoint.IsBlocked)
                    checkpoint.IsBlocked = false;
                await _checkpointRepository.UpdateCheckpointAsync(checkpoint);

                return Ok($"Checkpoint with ID {checkpointId} is blocked.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpPost("UnlockCheckpoints")]
        public async Task<IActionResult> UnlockCheckpoints()
        {
            try
            {
                var checkpoints = await _checkpointRepository.FindCheckpointsAsync();
                foreach (var checkpoint in checkpoints)
                {
                    checkpoint.IsActive = false;
                }

                await _checkpointRepository.UpdateCheckpointsAsync(checkpoints);

                return Ok("All checkpoints are unlocked.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpGet("SuccessfulAttempts")]
        public async Task<IActionResult> GetSuccessfulAttempts(DateTime date)
        {
            try
            {
                var passingAttempts = await _checkpointPassingAttemptsRepository.FindCheckpointPassingAttemptsAsync(
                    attemptTime: date.Date, accessed: true);
                return PartialView("AttemptsPartial", passingAttempts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        [HttpGet("FailedAttempts")]
        public async Task<IActionResult> GetFailedAttempts(DateTime date)
        {
            try
            {
                var passingAttempts = await _checkpointPassingAttemptsRepository.FindCheckpointPassingAttemptsAsync(
                    attemptTime: date.Date, accessed: false);
                return PartialView("AttemptsPartial", passingAttempts);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
    }
}
