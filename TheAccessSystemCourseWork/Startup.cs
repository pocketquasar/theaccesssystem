﻿using System.Reflection.Metadata;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;
using TheAccessSystemCourseWork.Controllers;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Implementations;
using TheAccessSystemCourseWork.Repositories.Interfaces;
using TheAccessSystemCourseWork.Services;

namespace TheAccessSystemCourseWork;

public class Startup
{
    public IConfiguration Configuration { get; }
    
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }
    

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDbContext<AccessSystemContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("development")));
        
        services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromDays(7);
                options.LoginPath = "/User/Login";
                options.LogoutPath = "/User/Logout";
            });
        
        
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IPassRepository, PassRepository>();
        services.AddScoped<IRightsLevelRepository, RightsLevelRepository>();
        services.AddScoped<IPassRightsRepository, PassRightsRepository>();
        services.AddScoped<IPassOperationRepository, PassOperationRepository>();
        services.AddScoped<IFactoryAreaRepository, FactoryAreaRepository>();
        services.AddScoped<IEmployeeRepository, EmployeeRepository>();
        services.AddScoped<ICheckpointRightsRepository, CheckpointRightsRepository>();
        services.AddScoped<ICheckpointRepository, CheckpointRepository>();
        services.AddScoped<ICheckpointPassingAttemptsRepository, CheckpointPassingAttemptsRepository>();
        services.AddScoped<IGuestRepository, GuestRepository>();
        services.AddScoped<AuthService>();
        services.AddScoped<BuildingService>(s => new BuildingService(".\\wwwroot\\images\\"));
        services.AddMvc();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseHsts();
        }
        
        app.UseAuthentication();
        app.UseHttpsRedirection();
        app.UseRouting();
        app.UseAuthorization();
        app.UseStaticFiles();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}