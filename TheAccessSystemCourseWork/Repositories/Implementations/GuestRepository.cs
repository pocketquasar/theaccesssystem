﻿using Microsoft.EntityFrameworkCore;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

public class GuestRepository : BaseRepository, IGuestRepository
{
    public GuestRepository(AccessSystemContext context) 
        : base(context) {}
    
    public async Task<Guest?> GetGuestByUserIDAsync(Guid userID)
    {
        return await _context.Guests
            .Include(e => e.User)
            .ThenInclude(u => u.Pass)
            .Where(e => e.UserID == userID)
            .FirstOrDefaultAsync();
    }
    
    public async Task<Guest?> GetGuestByPassport(Int64 passport)
    {
        return await _context.Guests
            .Include(g => g.User)
            .ThenInclude(g => g.Pass)
            .Where(g => g.Passport == passport)
            .FirstOrDefaultAsync();
    }

    public async Task<bool> CreateGuestAsync(Guest guest)
    {
        if (guest == null)
            throw new ArgumentNullException(nameof(guest));

        await _context.Guests.AddAsync(guest);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateGuestAsync(Guest updatedGuest)
    {
        if (updatedGuest == null)
            throw new ArgumentNullException(nameof(updatedGuest));

        var existingGuest = await _context.Guests.FindAsync(updatedGuest.Passport);
        if (existingGuest == null)
            return false;

        existingGuest.FirstName = updatedGuest.FirstName;
        existingGuest.SecondName = updatedGuest.SecondName;  
        existingGuest.MiddleName = updatedGuest.MiddleName;
        existingGuest.PhoneNumber = updatedGuest.PhoneNumber;
        existingGuest.UserID = updatedGuest.UserID;
        existingGuest.User = updatedGuest.User;
            
        _context.Guests.Update(existingGuest);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeleteGuestAsync(Int64 passport)
    {
        var guest = await _context.Guests.FindAsync(passport);
        if (guest == null)
            return false;

        _context.Guests.Remove(guest);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<Guest>> FindGuestsAsync(
        string? firstName = null,
        string? secondName = null,
        string? middleName = null,
        Int64? passport = null,
        string? phoneNumber = null)
    {
        var query = _context.Guests
            .Include(g => g.User)
            .ThenInclude(g => g.Pass)
            .AsQueryable();

        if (passport != null)
        {
            query = query.Where(g => g.Passport == passport);
        }
        
        if (firstName != null)
        {
            query = query.Where(g => g.FirstName == firstName);
        }
        
        if (secondName != null)
        {
            query = query.Where(g => g.SecondName == secondName);
        }
        
        if (middleName != null)
        {
            query = query.Where(g => g.MiddleName == middleName);
        }
        
        if (middleName != null)
        {
            query = query.Where(g => g.PhoneNumber == phoneNumber);
        }
        
        return await query.ToListAsync();
    }
}