﻿using Microsoft.EntityFrameworkCore;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

public class FactoryAreaRepository : BaseRepository, IFactoryAreaRepository
{
    public FactoryAreaRepository(AccessSystemContext context) 
        : base(context) {}

    public async Task<bool> AddFactoryAreaAsync(FactoryArea newFactoryArea)
    {
        if (newFactoryArea == null)
            throw new ArgumentNullException(nameof(newFactoryArea));

        await _context.FactoryAreas.AddAsync(newFactoryArea);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<FactoryArea?> GetFactoryAreaByIdAsync(int id)
    {
        return await _context.FactoryAreas
            .Include(fa => fa.Checkpoints)
            .FirstOrDefaultAsync(fa => fa.ID == id);
    }

    public async Task<bool> UpdateFactoryAreaAsync(FactoryArea updatedFactoryArea)
    {
        if (updatedFactoryArea == null)
            throw new ArgumentNullException(nameof(updatedFactoryArea));

        var existingFactoryArea = await _context.FactoryAreas.FindAsync(updatedFactoryArea.ID);
        if (existingFactoryArea == null)
            return false;

        existingFactoryArea.Building = updatedFactoryArea.Building;
        existingFactoryArea.Floor = updatedFactoryArea.Floor;
        existingFactoryArea.Name = updatedFactoryArea.Name;

        _context.FactoryAreas.Update(existingFactoryArea);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeleteFactoryAreaAsync(int id)
    {
        var factoryArea = await _context.FactoryAreas.FindAsync(id);
        if (factoryArea == null)
            return false;

        _context.FactoryAreas.Remove(factoryArea);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<FactoryArea>> FindFactoryAreasAsync(
        string? building = null,
        int? floor = null,
        string? name = null)
    {
        var query = _context.FactoryAreas
            .AsQueryable();

        if (building != null)
        {
            query = query.Where(fa => fa.Building == building);
        }

        if (floor.HasValue)
        {
            query = query.Where(fa => fa.Floor == floor.Value);
        }

        if (!string.IsNullOrWhiteSpace(name))
        {
            query = query.Where(fa => fa.Name.Contains(name));
        }

        return await query.ToListAsync();
    }
}