﻿using Microsoft.EntityFrameworkCore;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

public class EmployeeRepository : BaseRepository, IEmployeeRepository
{
    public EmployeeRepository(AccessSystemContext context) 
        : base(context) {}

    public async Task<Employee?> GetEmployeeByUserIDAsync(Guid userID)
    {
        return await _context.Employees
            .Include(e => e.User)
            .ThenInclude(u => u.Pass)
            .Where(e => e.UserID == userID)
            .FirstOrDefaultAsync();
    }
    
    public async Task<Employee?> GetEmployeeByServiceNumber(int serviceNumber)
    {
        return await _context.Employees
            .Include(e => e.User)
            .ThenInclude(u => u.Pass)
            .Where(e => e.ServiceNumber == serviceNumber)
            .FirstOrDefaultAsync();
    }

    public async Task<bool> CreateEmployeeAsync(Employee employee)
    {
        if (employee == null)
            throw new ArgumentNullException(nameof(employee));

        await _context.Employees.AddAsync(employee);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateEmployeeAsync(Employee updatedEmployee)
    {
        if (updatedEmployee == null)
            throw new ArgumentNullException(nameof(updatedEmployee));

        var existingEmployee = await _context.Employees.FindAsync(updatedEmployee.ServiceNumber);
        if (existingEmployee == null)
            return false;

        existingEmployee.FirstName = updatedEmployee.FirstName;
        existingEmployee.SecondName = updatedEmployee.SecondName;
        existingEmployee.MiddleName = updatedEmployee.MiddleName;
        existingEmployee.BirthDate = updatedEmployee.BirthDate;
        existingEmployee.Position = updatedEmployee.Position;
        existingEmployee.Department = updatedEmployee.Department;
        existingEmployee.PhoneNumber = updatedEmployee.PhoneNumber;

        _context.Employees.Update(existingEmployee);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeleteEmployeeAsync(int serviceNumber)
    {
        var employee = await _context.Employees.FindAsync(serviceNumber);
        if (employee == null)
            return false;

        _context.Employees.Remove(employee);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<Employee>> FindEmployeesAsync(
        int? serviceNumber = null,
        string? firstName = null,
        string? secondName = null,
        string? position = null,
        int? department = null,
        DateTime? birthDate = null)
    {
        var query = _context.
            Employees
            .Include(e => e.User)
            .ThenInclude(u => u.Pass)
            .AsQueryable();

        if (serviceNumber != null)
        {
            query = query.Where(e => e.ServiceNumber == serviceNumber.Value);
        }
        
        if (firstName != null)
        {
            query = query.Where(e => e.FirstName.Contains(firstName));
        }

        if (secondName != null)
        {
            query = query.Where(e => e.SecondName.Contains(secondName));
        }

        if (position != null)
        {
            query = query.Where(e => e.Position.Contains(position));
        }

        if (department != null)
        {
            query = query.Where(e => e.Department == department);
        }

        if (birthDate != null)
        {
            query = query.Where(e => e.BirthDate.Date == birthDate.Value.Date);
        }

        return await query.ToListAsync();
    }
}
