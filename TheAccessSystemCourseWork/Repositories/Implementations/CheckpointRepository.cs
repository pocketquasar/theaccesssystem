﻿using Microsoft.EntityFrameworkCore;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

public class CheckpointRepository : BaseRepository, ICheckpointRepository
{
    public CheckpointRepository(AccessSystemContext context) 
        : base(context) {}

    public async Task<bool> AddCheckpointAsync(Checkpoint checkpoint)
    {
        if (checkpoint == null)
            throw new ArgumentNullException(nameof(checkpoint));

        checkpoint.FactoryArea = await _context.FactoryAreas.FirstOrDefaultAsync(fa => fa.ID == checkpoint.AreaID);
        await _context.Checkpoints.AddAsync(checkpoint);
        
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<Checkpoint?> GetCheckpointByIdAsync(string checkpointId)
    {
        return await _context.Checkpoints
            .Include(cp => cp.CheckpointRights)
            .ThenInclude(cr => cr.RightsLevel)
            .Include(cp => cp.FactoryArea)
            .FirstOrDefaultAsync();
    }

    public async Task<bool> UpdateCheckpointAsync(Checkpoint updatedCheckpoint)
    {
        if (updatedCheckpoint == null)
            throw new ArgumentNullException(nameof(updatedCheckpoint));

        var existingCheckpoint = await _context.Checkpoints.FindAsync(updatedCheckpoint.ID);
        if (existingCheckpoint == null)
            return false;

        existingCheckpoint.AreaID = updatedCheckpoint.AreaID;
        existingCheckpoint.FactoryArea = updatedCheckpoint.FactoryArea;
        existingCheckpoint.IsActive = updatedCheckpoint.IsActive;
        existingCheckpoint.IsBlocked = updatedCheckpoint.IsBlocked;
        existingCheckpoint.CheckpointRights = updatedCheckpoint.CheckpointRights;

        _context.Checkpoints.Update(existingCheckpoint);
        return await _context.SaveChangesAsync() > 0;
    }
    
    public async Task<bool> UpdateCheckpointsAsync(List<Checkpoint> updatedCheckpoints)
    {
        try
        {
            foreach (var updatedCheckpoint in updatedCheckpoints)
            {
                var existingCheckpoint = await _context.Checkpoints.FindAsync(updatedCheckpoint.ID);
                if (existingCheckpoint == null)
                {
                    continue;
                }

                existingCheckpoint.AreaID = updatedCheckpoint.AreaID;

                _context.Checkpoints.Update(existingCheckpoint);
            }

            return await _context.SaveChangesAsync() > 0;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating checkpoints: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> DeleteCheckpointAsync(string checkpointId)
    {
        var checkpoint = await _context.Checkpoints.FindAsync(checkpointId);
        if (checkpoint == null)
            return false;

        _context.Checkpoints.Remove(checkpoint);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<Checkpoint>> FindCheckpointsAsync(
        int? areaId = null,
        bool? hasRights = null)
    {
        var query = _context.Checkpoints
            .Include(cp => cp.CheckpointRights)
            .ThenInclude(cr => cr.RightsLevel)
            .Include(cp => cp.FactoryArea)
            .AsQueryable();

        if (areaId.HasValue)
        {
            query = query.Where(cp => cp.AreaID == areaId);
        }

        if (hasRights.HasValue)
        {
            if (hasRights.Value)
            {
                query = query.Where(cp => cp.CheckpointRights.Any());
            }
            else
            {
                query = query.Where(cp => !cp.CheckpointRights.Any());
            }
        }

        return await query.ToListAsync();
    }
}