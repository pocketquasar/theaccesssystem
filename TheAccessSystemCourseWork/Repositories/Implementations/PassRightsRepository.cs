﻿using Microsoft.EntityFrameworkCore;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

public class PassRightsRepository : BaseRepository, IPassRightsRepository
{
    public PassRightsRepository(AccessSystemContext context) 
        : base(context) {}

    public async Task<bool> AddPassRightsAsync(PassRights newPassRights)
    {
        if (newPassRights == null)
            throw new ArgumentNullException(nameof(newPassRights));

        await _context.PassRights.AddAsync(newPassRights);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<PassRights?> GetPassRightsByIdAsync(int id)
    {
        return await _context.PassRights
            .Include(pr => pr.Pass)
            .Include(pr => pr.RightsLevel)
            .FirstOrDefaultAsync(pr => pr.ID == id);
    }

    public async Task<bool> UpdatePassRightsAsync(PassRights updatedPassRights)
    {
        if (updatedPassRights == null)
            throw new ArgumentNullException(nameof(updatedPassRights));

        var existingPassRights = await _context.PassRights.FindAsync(updatedPassRights.ID);
        if (existingPassRights == null)
            return false;

        existingPassRights.PassID = updatedPassRights.PassID;
        existingPassRights.RightsLevelID = updatedPassRights.RightsLevelID;

        _context.PassRights.Update(existingPassRights);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeletePassRightsAsync(int id)
    {
        var passRights = await _context.PassRights.FindAsync(id);
        if (passRights == null)
            return false;

        _context.PassRights.Remove(passRights);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<PassRights>> FindPassRightsAsync(
        Guid? passId = null,
        string? rightsLevelId = null)
    {
        var query = _context.PassRights.AsQueryable();

        if (passId.HasValue)
        {
            query = query.Where(pr => pr.PassID == passId.Value);
        }

        if (!string.IsNullOrWhiteSpace(rightsLevelId))
        {
            query = query.Where(pr => pr.RightsLevelID == rightsLevelId);
        }

        return await query.ToListAsync();
    }
}