﻿using Microsoft.EntityFrameworkCore;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

public class PassOperationRepository : BaseRepository, IPassOperationRepository
{
    public PassOperationRepository(AccessSystemContext context) 
        : base(context) {}

    public async Task<bool> AddPassOperationAsync(PassOperation newOperation)
    {
        if (newOperation == null)
            throw new ArgumentNullException(nameof(newOperation));

        await _context.PassOperations.AddAsync(newOperation);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<PassOperation?> GetPassOperationByIdAsync(Guid operationId)
    {
        return await _context.PassOperations
            .Include(po => po.Pass)
            .Include(po => po.Employee)
            .FirstOrDefaultAsync(po => po.ID == operationId);
    }

    public async Task<bool> UpdatePassOperationAsync(PassOperation updatedOperation)
    {
        if (updatedOperation == null)
            throw new ArgumentNullException(nameof(updatedOperation));

        var existingOperation = await _context.PassOperations.FindAsync(updatedOperation.ID);
        if (existingOperation == null)
            return false;

        existingOperation.OperationType = updatedOperation.OperationType;
        existingOperation.PassID = updatedOperation.PassID;
        existingOperation.EmployeeNumber = updatedOperation.EmployeeNumber;

        _context.PassOperations.Update(existingOperation);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeletePassOperationAsync(Guid operationId)
    {
        var operation = await _context.PassOperations.FindAsync(operationId);
        if (operation == null)
            return false;

        _context.PassOperations.Remove(operation);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<PassOperation>> FindPassOperationsAsync(
        string? operationType = null,
        Guid? passId = null,
        int? employeeNumber = null,
        DateTime? createdAt = null)
    {
        var query = _context.PassOperations
            .Include(po => po.Pass)
            .Include(po => po.Employee)
            .AsQueryable();

        if (!string.IsNullOrWhiteSpace(operationType))
        {
            query = query.Where(po => po.OperationType == operationType);
        }

        if (passId.HasValue)
        {
            query = query.Where(po => po.PassID == passId.Value);
        }

        if (employeeNumber.HasValue)
        {
            query = query.Where(po => po.EmployeeNumber == employeeNumber.Value);
        }

        if (createdAt.HasValue)
        {
            query = query.Where(po => po.CreatedAt.Date == createdAt.Value.Date);
        }

        return await query.ToListAsync();
    }
}