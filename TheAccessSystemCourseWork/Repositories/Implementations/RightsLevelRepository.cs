﻿using Microsoft.EntityFrameworkCore;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

public class RightsLevelRepository : BaseRepository, IRightsLevelRepository
{
    public RightsLevelRepository(AccessSystemContext context) 
        : base(context) {}

    public async Task<bool> AddRightsLevelAsync(RightsLevel rightsLevel)
    {
        if (rightsLevel == null)
            throw new ArgumentNullException(nameof(rightsLevel));

        await _context.RightsLevels.AddAsync(rightsLevel);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<RightsLevel?> GetRightsLevelByIdAsync(string rightsLevelId)
    {
        return await _context.RightsLevels
            .FirstOrDefaultAsync(rl => rl.ID == rightsLevelId);
    }

    public async Task<bool> UpdateRightsLevelAsync(RightsLevel updatedRightsLevel)
    {
        if (updatedRightsLevel == null)
            throw new ArgumentNullException(nameof(updatedRightsLevel));

        var existingRightsLevel = await _context.RightsLevels.FindAsync(updatedRightsLevel.ID);
        if (existingRightsLevel == null)
            return false;

        existingRightsLevel.Description = updatedRightsLevel.Description;
        existingRightsLevel.RequiredDepartment = updatedRightsLevel.RequiredDepartment;
        existingRightsLevel.RequiredPosition = updatedRightsLevel.RequiredPosition;

        _context.RightsLevels.Update(existingRightsLevel);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeleteRightsLevelAsync(string rightsLevelId)
    {
        var rightsLevel = await _context.RightsLevels.FindAsync(rightsLevelId);
        if (rightsLevel == null)
            return false;

        _context.RightsLevels.Remove(rightsLevel);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<RightsLevel>> FindRightsLevelsAsync(
        int? requiredDepartment = null,
        string? requiredPosition = null)
    {
        var query = _context.RightsLevels.AsQueryable();

        if (requiredDepartment.HasValue)
        {
            query = query.Where(rl => rl.RequiredDepartment == requiredDepartment.Value);
        }

        if (!string.IsNullOrWhiteSpace(requiredPosition))
        {
            query = query.Where(rl => rl.RequiredPosition == requiredPosition);
        }

        return await query.ToListAsync();
    }
}