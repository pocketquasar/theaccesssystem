﻿using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

public class CheckpointRightsRepository : BaseRepository, ICheckpointRightsRepository
{
    public CheckpointRightsRepository(AccessSystemContext context) 
        : base(context) {}

    public async Task<bool> AddCheckpointRightsAsync(CheckpointRights newRights)
    {
        if (newRights == null)
            throw new ArgumentNullException(nameof(newRights));

        await _context.CheckpointRights.AddAsync(newRights);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<CheckpointRights?> GetCheckpointRightsByIdAsync(int id)
    {
        return await _context.CheckpointRights
            .Include(cr => cr.Checkpoint)
            .Include(cr => cr.RightsLevel)
            .FirstOrDefaultAsync(cr => cr.ID == id);
    }

    public async Task<bool> UpdateCheckpointRightsAsync(CheckpointRights updatedRights)
    {
        if (updatedRights == null)
            throw new ArgumentNullException(nameof(updatedRights));

        var existingRights = await _context.CheckpointRights.FindAsync(updatedRights.ID);
        if (existingRights == null)
            return false;

        existingRights.CheckpointID = updatedRights.CheckpointID;
        existingRights.RightsLevelID = updatedRights.RightsLevelID;

        _context.CheckpointRights.Update(existingRights);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeleteCheckpointRightsAsync(int id)
    {
        var rights = await _context.CheckpointRights.FindAsync(id);
        if (rights == null)
            return false;

        _context.CheckpointRights.Remove(rights);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<CheckpointRights>> FindCheckpointRightsAsync(
        string? checkpointId = null,
        string? rightsLevelId = null)
    {
        var query = _context.CheckpointRights.AsQueryable();

        if (!string.IsNullOrWhiteSpace(checkpointId))
        {
            query = query.Where(cr => cr.CheckpointID == checkpointId);
        }

        if (!string.IsNullOrWhiteSpace(rightsLevelId))
        {
            query = query.Where(cr => cr.RightsLevelID == rightsLevelId);
        }

        return await query.ToListAsync();
    }
}
