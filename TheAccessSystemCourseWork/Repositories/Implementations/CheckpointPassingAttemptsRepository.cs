﻿using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

public class CheckpointPassingAttemptsRepository : BaseRepository, ICheckpointPassingAttemptsRepository
{
    public CheckpointPassingAttemptsRepository(AccessSystemContext context) 
        : base(context) {}
    
    public async Task<bool> AddCheckpointPassingAttemptAsync(CheckpointPassingAttempts newAttempt)
    {
        if (newAttempt == null)
            throw new ArgumentNullException(nameof(newAttempt));

        await _context.CheckpointPassingAttempts.AddAsync(newAttempt);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<CheckpointPassingAttempts?> GetCheckpointPassingAttemptByIdAsync(Guid id)
    {
        return await _context.CheckpointPassingAttempts
            .Include(cpa => cpa.Checkpoint)
            .Include(cpa => cpa.Pass)
            .FirstOrDefaultAsync(cpa => cpa.ID == id);
    }

    public async Task<bool> UpdateCheckpointPassingAttemptAsync(CheckpointPassingAttempts updatedAttempt)
    {
        if (updatedAttempt == null)
            throw new ArgumentNullException(nameof(updatedAttempt));

        var existingAttempt = await _context.CheckpointPassingAttempts.FindAsync(updatedAttempt.ID);
        if (existingAttempt == null)
            return false;

        existingAttempt.CheckpointID = updatedAttempt.CheckpointID;
        existingAttempt.PassID = updatedAttempt.PassID;
        existingAttempt.Accessed = updatedAttempt.Accessed;
        existingAttempt.AttemptTime = updatedAttempt.AttemptTime;

        _context.CheckpointPassingAttempts.Update(existingAttempt);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeleteCheckpointPassingAttemptAsync(Guid id)
    {
        var attempt = await _context.CheckpointPassingAttempts.FindAsync(id);
        if (attempt == null)
            return false;

        _context.CheckpointPassingAttempts.Remove(attempt);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<CheckpointPassingAttempts>> FindCheckpointPassingAttemptsAsync(
        Guid? passId = null,
        string? checkpointId = null,
        bool? accessed = null,
        DateTime? attemptTime = null)
    {
        var query = _context.CheckpointPassingAttempts.AsQueryable();

        if (passId.HasValue)
        {
            query = query.Where(cpa => cpa.PassID == passId.Value);
        }

        if (!string.IsNullOrWhiteSpace(checkpointId))
        {
            query = query.Where(cpa => cpa.CheckpointID == checkpointId);
        }

        if (accessed.HasValue)
        {
            query = query.Where(cpa => cpa.Accessed == accessed.Value);
        }

        if (attemptTime.HasValue)
        {
            query = query.Where(cpa => cpa.AttemptTime.Date == attemptTime.Value.Date);
        }

        return await query.ToListAsync();
    }
}
