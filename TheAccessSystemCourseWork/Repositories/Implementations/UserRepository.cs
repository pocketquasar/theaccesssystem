﻿using Microsoft.EntityFrameworkCore;
using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

public class UserRepository : BaseRepository, IUserRepository
{

    public UserRepository(AccessSystemContext context) 
        : base(context) {}

    public async Task<User?> GetUserByLoginAsync(string login)
    {
        return await _context.Users
            .Where(u => u.Login == login)
            .FirstOrDefaultAsync();
    }

    public async Task<User?> GetUserByIdAsync(Guid userID)
    {
        return await _context.Users
            .Include(u => u.Pass)
            .ThenInclude(p => p.PassRights)
            .ThenInclude(pr => pr.RightsLevel)
            .FirstOrDefaultAsync(u => u.ID == userID);
    }

    public async Task<bool> CreateUserAsync(User user)
    {
        if (user == null)
            throw new ArgumentNullException(nameof(user));

        await _context.Users.AddAsync(user);
        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdatePasswordAsync(Guid userId, string newPassword)
    {
        if (string.IsNullOrWhiteSpace(newPassword))
            throw new ArgumentException("New password cannot be empty.", nameof(newPassword));

        var user = await _context.Users.FindAsync(userId);
        if (user == null)
            return false;

        user.Password = newPassword;
        _context.Users.Update(user);

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdateUserAsync(User user)
    {
        var existingUser = await _context.Users.FirstOrDefaultAsync(u => u.ID == user.ID);

        if (existingUser == null)
            return false;

        existingUser.Password = user.Password;
        existingUser.Login = user.Login;
        existingUser.Pass = user.Pass;
        existingUser.Role = user.Role;
        existingUser.PassID = user.PassID;
        
        _context.Users.Update(existingUser);

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> UpdatePassAsync(Guid userId, Guid newPassId)
    {
        var user = await _context.Users.FindAsync(userId);
        if (user == null)
            return false;

        user.PassID = newPassId;
        user.Pass = await _context.Passes.FindAsync(newPassId);

        _context.Users.Update(user);

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<List<User>> FindUsersAsync(Roles? roles = null)
    {
        var query = _context.Users.AsQueryable();

        if (roles.HasValue)
        {
            query = query.Where(u => u.Role == roles.Value);
        }

        return await query.ToListAsync();
    }
    
    public async Task<List<User>> FindUsersWithAllInfoAsync(Guid? ID = null, Roles? roles = null, Guid? passID = null)
    {
        var query = _context.
            Users.
            AsQueryable();

        if (ID.HasValue)
        {
            query = query.Where(u => u.ID == ID.Value);
        }
        if (roles.HasValue)
        {
            query = query.Where(u => u.Role == roles.Value);
        }
        if (passID.HasValue)
        {
            query = query.Where(u => u.PassID == passID.Value);
        }

        return await query.ToListAsync();
    }
}