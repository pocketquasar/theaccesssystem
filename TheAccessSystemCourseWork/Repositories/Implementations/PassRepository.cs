﻿using TheAccessSystemCourseWork.Models;
using TheAccessSystemCourseWork.Repositories.Interfaces;

namespace TheAccessSystemCourseWork.Repositories.Implementations;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

public class PassRepository : BaseRepository, IPassRepository
{
    public PassRepository(AccessSystemContext context) 
        : base(context) {}

    public async Task<bool> CreatePassAsync(Pass newPass)
    {
        if (newPass == null)
            throw new ArgumentNullException(nameof(newPass));

        newPass.IsActive = true;
        await _context.Passes.AddAsync(newPass);
        return await _context.SaveChangesAsync() > 0;
    }
    
    public async Task<Pass?> GetPassByIdAsync(Guid passId)
    {
        return await _context.Passes
            .Include(p => p.PassRights) 
            .FirstOrDefaultAsync(p => p.ID == passId);
    }

    public async Task<bool> UpdatePassAsync(Pass updatedPass)
    {
        if (updatedPass == null)
            throw new ArgumentNullException(nameof(updatedPass));

        var existingPass = await _context.Passes.FindAsync(updatedPass.ID);
        if (existingPass == null)
            return false;

        existingPass.Type = updatedPass.Type;
        existingPass.IsActive = updatedPass.IsActive;
        existingPass.ExpiredAt = updatedPass.ExpiredAt;

        _context.Passes.Update(existingPass);

        return await _context.SaveChangesAsync() > 0;
    }

    public async Task<bool> DeletePassAsync(Guid passId)
    {
        var pass = await _context.Passes.FindAsync(passId);
        if (pass == null)
            return false;

        _context.Passes.Remove(pass);
        return await _context.SaveChangesAsync() > 0;

    }

    public async Task<List<Pass>> FindPassesAsync(
        bool? isAttached = null,
        bool? isActive = null,
        PassType? type = null,
        DateTime? expiredAt = null)
    {
        var query = _context.Passes.AsQueryable();
        
        if (isAttached.HasValue)
        {
            query = query.Where(p => p.IsAttached == isAttached.Value);
        }
        
        if (isActive.HasValue)
        {
            query = query.Where(p => p.IsActive == isActive.Value);
        }

        if (type.HasValue)
        {
            query = query.Where(p => p.Type == type.Value);
        }

        if (expiredAt.HasValue)
        {
            query = query.Where(p => p.ExpiredAt.Date == expiredAt.Value.Date);
        }

        return await query.ToListAsync();
    }
}
