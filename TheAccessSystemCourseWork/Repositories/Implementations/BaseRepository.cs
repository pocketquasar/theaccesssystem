﻿using TheAccessSystemCourseWork.Models;
namespace TheAccessSystemCourseWork.Repositories.Implementations;

public class BaseRepository
{
    protected AccessSystemContext _context;

    protected BaseRepository(AccessSystemContext context)
    {
        _context = context;
    }
}