﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface IGuestRepository
{
    Task<Guest?> GetGuestByUserIDAsync(Guid userID);
    Task<Guest?> GetGuestByPassport(Int64 passport);
    Task<bool> CreateGuestAsync(Guest employee);
    Task<bool> UpdateGuestAsync(Guest updatedEmployee);
    Task<bool> DeleteGuestAsync(Int64 passport);

    Task<List<Guest>> FindGuestsAsync(
        string? firstName = null,
        string? secondName = null,
        string? middleName = null,
        Int64? passport = null,
        string? phoneNumber = null);
}