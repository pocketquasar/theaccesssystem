﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface ICheckpointPassingAttemptsRepository
{
    Task<bool> AddCheckpointPassingAttemptAsync(CheckpointPassingAttempts newAttempt);
    Task<CheckpointPassingAttempts?> GetCheckpointPassingAttemptByIdAsync(Guid id);
    Task<bool> UpdateCheckpointPassingAttemptAsync(CheckpointPassingAttempts updatedAttempt);
    Task<bool> DeleteCheckpointPassingAttemptAsync(Guid id);

    Task<List<CheckpointPassingAttempts>> FindCheckpointPassingAttemptsAsync(
        Guid? passId = null,
        string? checkpointId = null,
        bool? accessed = null,
        DateTime? attemptTime = null);
}