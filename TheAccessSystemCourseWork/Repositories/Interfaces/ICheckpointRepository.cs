﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface ICheckpointRepository
{
    Task<bool> AddCheckpointAsync(Checkpoint checkpoint);
    Task<Checkpoint?> GetCheckpointByIdAsync(string checkpointId);
    Task<bool> UpdateCheckpointAsync(Checkpoint updatedCheckpoint);
    Task<bool> DeleteCheckpointAsync(string checkpointId);
    Task<bool> UpdateCheckpointsAsync(List<Checkpoint> updatedCheckpoints);

    Task<List<Checkpoint>> FindCheckpointsAsync(
        int? areaId = null,
        bool? hasRights = null);
}