﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface ICheckpointRightsRepository
{
    Task<bool> AddCheckpointRightsAsync(CheckpointRights newRights);
    Task<CheckpointRights?> GetCheckpointRightsByIdAsync(int id);
    Task<bool> UpdateCheckpointRightsAsync(CheckpointRights updatedRights);
    Task<bool> DeleteCheckpointRightsAsync(int id);

    Task<List<CheckpointRights>> FindCheckpointRightsAsync(
        string? checkpointId = null,
        string? rightsLevelId = null);
}