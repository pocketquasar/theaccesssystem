﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface IPassRepository
{
    Task<bool> CreatePassAsync(Pass newPass);
    Task<Pass?> GetPassByIdAsync(Guid passId);
    Task<bool> UpdatePassAsync(Pass updatedPass);
    Task<bool> DeletePassAsync(Guid passId);

    Task<List<Pass>> FindPassesAsync(
        bool? isAttached = null,
        bool? isActive = null,
        PassType? type = null,
        DateTime? expiredAt = null);
}