﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface IUserRepository
{
    Task<User?> GetUserByLoginAsync(string login);
    Task<bool> CreateUserAsync(User user);
    Task<bool> UpdatePasswordAsync(Guid userId, string newPassword);
    Task<bool> UpdatePassAsync(Guid userId, Guid newPassId);
    Task<User?> GetUserByIdAsync(Guid userID);
    Task<bool> UpdateUserAsync(User user);
    Task<List<User>> FindUsersAsync(Roles? roles = null);
}