﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface IEmployeeRepository
{
    Task<Employee?> GetEmployeeByUserIDAsync(Guid userID);
    Task<Employee?> GetEmployeeByServiceNumber(int serviceNumber);
    Task<bool> CreateEmployeeAsync(Employee employee);
    Task<bool> UpdateEmployeeAsync(Employee updatedEmployee);
    Task<bool> DeleteEmployeeAsync(int serviceNumber);

    Task<List<Employee>> FindEmployeesAsync(
        int? serviceNumber = null,
        string? firstName = null,
        string? secondName = null,
        string? position = null,
        int? department = null,
        DateTime? birthDate = null);
}