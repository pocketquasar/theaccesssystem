﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface IPassOperationRepository
{
    Task<bool> AddPassOperationAsync(PassOperation newOperation);
    Task<PassOperation?> GetPassOperationByIdAsync(Guid operationId);
    Task<bool> UpdatePassOperationAsync(PassOperation updatedOperation);
    Task<bool> DeletePassOperationAsync(Guid operationId);

    Task<List<PassOperation>> FindPassOperationsAsync(
        string? operationType = null,
        Guid? passId = null,
        int? employeeNumber = null,
        DateTime? createdAt = null);
}