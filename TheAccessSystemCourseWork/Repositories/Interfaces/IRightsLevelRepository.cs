﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface IRightsLevelRepository
{
    Task<bool> AddRightsLevelAsync(RightsLevel rightsLevel);
    Task<RightsLevel?> GetRightsLevelByIdAsync(string rightsLevelId);
    Task<bool> UpdateRightsLevelAsync(RightsLevel updatedRightsLevel);
    Task<bool> DeleteRightsLevelAsync(string rightsLevelId);

    Task<List<RightsLevel>> FindRightsLevelsAsync(
        int? requiredDepartment = null,
        string? requiredPosition = null);
}