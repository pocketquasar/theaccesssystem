﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface IFactoryAreaRepository
{
    Task<bool> AddFactoryAreaAsync(FactoryArea newFactoryArea);
    Task<FactoryArea?> GetFactoryAreaByIdAsync(int id);
    Task<bool> UpdateFactoryAreaAsync(FactoryArea updatedFactoryArea);
    Task<bool> DeleteFactoryAreaAsync(int id);

    Task<List<FactoryArea>> FindFactoryAreasAsync(
        string? building = null,
        int? floor = null,
        string? name = null);
}