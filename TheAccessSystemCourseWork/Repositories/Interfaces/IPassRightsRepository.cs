﻿using TheAccessSystemCourseWork.Models;

namespace TheAccessSystemCourseWork.Repositories.Interfaces;

public interface IPassRightsRepository
{
    Task<bool> AddPassRightsAsync(PassRights newPassRights);
    Task<PassRights?> GetPassRightsByIdAsync(int id);
    Task<bool> UpdatePassRightsAsync(PassRights updatedPassRights);
    Task<bool> DeletePassRightsAsync(int id);

    Task<List<PassRights>> FindPassRightsAsync(
        Guid? passId = null,
        string? rightsLevelId = null);
}