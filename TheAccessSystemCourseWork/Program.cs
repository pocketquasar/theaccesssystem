using TheAccessSystemCourseWork;

public static class Program
{
	public static void Main(string[] args)
	{
		var builder = WebApplication.CreateBuilder(args);
		
		builder.Services.AddControllersWithViews();
		
		var startup = new Startup(builder.Configuration);
		startup.ConfigureServices(builder.Services);
		

		var app = builder.Build();

		startup.Configure(app, builder.Environment);
		
		app.MapControllerRoute(
			name: "default",
			pattern: "{controller=User}/{action=Login}/{id?}");
		
		app.Run();
	}
}
