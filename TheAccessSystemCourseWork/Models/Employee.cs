﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TheAccessSystemCourseWork.Models;

public class Employee
{
    [Key]
    [DisplayName("Табельный номер")]
    public int ServiceNumber { get; set; }
    [DisplayName("Идентификатор пользователя")]
    public Guid UserID { get; set; }
    [DisplayName("Пользователь")]
    public virtual User? User { get; set; }
    [DisplayName("Имя")]
    public string FirstName { get; set; }
    [DisplayName("Фамилия")]
    public string SecondName { get; set; }
    [DisplayName("Отчество")]
    public string MiddleName { get; set; }
    [DisplayName("Дата рождения")]
    public DateTime BirthDate { get; set; }
    [DisplayName("Должность")]
    public string Position { get; set; }
    [DisplayName("Подразделение")]
    public int Department { get; set; }
    [DisplayName("Номер телефона")]
    public string PhoneNumber { get; set; }
}