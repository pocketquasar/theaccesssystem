﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TheAccessSystemCourseWork.Models;

[DisplayName("Помещение")]
public class FactoryArea
{
    [Key]
    [DisplayName("Идентификатор помещения")]
    public int ID { get; set; }
    [DisplayName("Здание")]
    public string Building { get; set; }
    [DisplayName("Этаж")]
    public int Floor { get; set; }
    [DisplayName("Название помещения")]
    public string Name { get; set; }
    [DisplayName("Контрольные точки")]
    public virtual ICollection<Checkpoint>? Checkpoints { get; set; } = new HashSet<Checkpoint>();
}