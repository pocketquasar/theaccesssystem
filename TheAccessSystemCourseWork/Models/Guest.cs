﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TheAccessSystemCourseWork.Models;

public class Guest
{
    [Key]
    [DisplayName("Паспортные данные")]
    public Int64 Passport { get; set; }
    [DisplayName("Имя")]
    public string FirstName { get; set; }
    [DisplayName("Фамилия")]
    public string SecondName { get; set; }
    [DisplayName("Отчество")]
    public string MiddleName { get; set; }
    [DisplayName("Идентификатор пользователя")]
    public Guid UserID { get; set; }
    [DisplayName("Пользователь")]
    public virtual User? User { get; set; }
    
    [DisplayName("Номер телефона")]
    public string PhoneNumber { get; set; }
}