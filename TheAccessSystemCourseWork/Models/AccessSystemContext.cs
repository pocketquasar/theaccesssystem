﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace TheAccessSystemCourseWork.Models;

public class AccessSystemContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Pass> Passes { get; set; }
    public DbSet<Employee> Employees { get; set; }
    public DbSet<RightsLevel> CheckpointRightsLevels { get; set; }
    public DbSet<CheckpointPassingAttempts> CheckpointPassingAttempts { get; set; }
    public DbSet<PassOperation> PassOperations { get; set; }
    public DbSet<FactoryArea> FactoryAreas { get; set; }
    public DbSet<PassRights> PassRights { get; set; }
    public DbSet<CheckpointRights> CheckpointRights { get; set; }
    public DbSet<Checkpoint> Checkpoints { get; set; }
    public DbSet<RightsLevel> RightsLevels { get; set; }
    public DbSet<Guest> Guests { get; set; }

    public AccessSystemContext(DbContextOptions<AccessSystemContext> opts)
        : base(opts)
    {
        Database.EnsureCreated();
    }
    
    public AccessSystemContext() {}
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>(UserConfigure);
        modelBuilder.Entity<Employee>(EmployeeConfigure);
        modelBuilder.Entity<Guest>(GuestConfigure);
        modelBuilder.Entity<CheckpointPassingAttempts>(CheckpointPassingAttemptsConfigure);
        modelBuilder.Entity<PassOperation>(PassOperationConfigure);
        modelBuilder.Entity<FactoryArea>(AreaConfigure);
        modelBuilder.Entity<RightsLevel>(RightsLevelConfigure);
        modelBuilder.Entity<PassRights>(PassRightsConfigure);
        modelBuilder.Entity<Pass>(PassConfigure);
        modelBuilder.Entity<CheckpointRights>(CheckpointRightsConfigure);
        modelBuilder.Entity<Checkpoint>(CheckpointConfigure);
    }

    public void AreaConfigure(EntityTypeBuilder<FactoryArea> builder)
    {
        builder
            .ToTable("FactoryAreas")
            .HasKey(fa => fa.ID);

        builder
            .Property(fa => fa.Name)
            .HasMaxLength(100);

        builder.Property(fa => fa.Building)
            .HasMaxLength(100);
        
        builder
            .HasMany(fa => fa.Checkpoints)
            .WithOne(c => c.FactoryArea)
            .HasForeignKey(c => c.AreaID)
            .OnDelete(DeleteBehavior.Cascade);
    }

    public void RightsLevelConfigure(EntityTypeBuilder<RightsLevel> builder)
    {
        builder
            .ToTable("RightsLevels")
            .HasKey(rl => rl.ID);
        
        builder
            .Property(rl => rl.ID)
            .HasMaxLength(12);
        
        builder
            .ToTable("RightsLevels")
            .Property(rl => rl.Description)
            .IsRequired()
            .HasMaxLength(200);
    }

    public void PassRightsConfigure(EntityTypeBuilder<PassRights> builder)
    {
        builder
            .ToTable("PassRights")
            .HasKey(pr => pr.ID);

        builder
            .Property(pr => pr.RightsLevelID)
            .HasMaxLength(12);
        
        builder
            .HasOne(pr => pr.Pass)
            .WithMany(p => p.PassRights)
            .HasForeignKey(pr => pr.PassID)
            .OnDelete(DeleteBehavior.NoAction);

        builder
            .HasOne(pr => pr.RightsLevel)
            .WithMany()
            .HasForeignKey(pr => pr.RightsLevelID)
            .OnDelete(DeleteBehavior.Restrict);
    }

    public void PassConfigure(EntityTypeBuilder<Pass> builder)
    {
        builder
            .ToTable("Passes")
            .HasKey(p => p.ID);
        
        builder
            .HasMany(p => p.PassRights)
            .WithOne(pr => pr.Pass);
    }

    public void CheckpointRightsConfigure(EntityTypeBuilder<CheckpointRights> builder)
    {
        builder
            .ToTable("CheckpointRights")
            .HasKey(cr => cr.ID);
        
        builder
            .Property(cr => cr.CheckpointID)
            .HasMaxLength(12);
        
        builder
            .Property(cr => cr.RightsLevelID)
            .HasMaxLength(12);
        
        builder
            .HasOne(cr => cr.Checkpoint)
            .WithMany(c => c.CheckpointRights)
            .HasForeignKey(cr => cr.CheckpointID);

        builder
            .HasOne(cr => cr.RightsLevel)
            .WithMany()
            .HasForeignKey(cr => cr.RightsLevelID);
    }

    public void PassOperationConfigure(EntityTypeBuilder<PassOperation> builder)
    {
        builder
            .ToTable("PassOperations")
            .HasKey(po => po.ID);

        builder
            .Property(po => po.OperationType)
            .HasMaxLength(25);
        
        builder
            .HasOne(po => po.Pass)
            .WithMany()
            .HasForeignKey(po => po.PassID)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasOne(po => po.Employee)
            .WithMany()
            .HasForeignKey(po => po.EmployeeNumber)
            .OnDelete(DeleteBehavior.Cascade);
    }

    public void CheckpointConfigure(EntityTypeBuilder<Checkpoint> builder)
    {
        builder
            .ToTable("Checkpoints")
            .HasKey(c => c.ID);
        
        builder
            .Property(c => c.ID)
            .HasMaxLength(12);
        
        builder
            .HasOne(c => c.FactoryArea)
            .WithMany(a => a.Checkpoints)
            .HasForeignKey(c => c.AreaID)
            .OnDelete(DeleteBehavior.Cascade);
    }

    public void CheckpointPassingAttemptsConfigure(EntityTypeBuilder<CheckpointPassingAttempts> builder)
    {
        builder
            .ToTable("CheckpointPassingAttempts")
            .HasKey(cpa => cpa.ID);

        builder
            .Property(cpa => cpa.CheckpointID)
            .HasMaxLength(12);
        
        builder
            .HasOne(cpa => cpa.Pass)
            .WithMany()
            .HasForeignKey(cpa => cpa.PassID)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .HasOne(cpa => cpa.Checkpoint)
            .WithMany()
            .HasForeignKey(cpa => cpa.CheckpointID)
            .OnDelete(DeleteBehavior.Restrict);
    }
    
    public void GuestConfigure(EntityTypeBuilder<Guest> builder)
    {
        builder
            .ToTable("Guests")
            .HasKey(e => e.Passport);

        builder
            .Property(g => g.Passport)
            .ValueGeneratedNever();
        
        builder
            .HasOne(g => g.User)
            .WithMany()
            .HasForeignKey(g => g.UserID)
            .OnDelete(DeleteBehavior.Cascade);
        
        builder
            .HasIndex(e => e.Passport)
            .IsUnique();

        builder
            .Property(e => e.FirstName)
            .IsRequired()
            .HasMaxLength(30);
        builder
            .Property(e => e.SecondName)
            .IsRequired()
            .HasMaxLength(30);
        builder
            .Property(e => e.MiddleName)
            .HasMaxLength(30);
        builder
            .Property(e => e.PhoneNumber)
            .HasMaxLength(10);
    }

    public void EmployeeConfigure(EntityTypeBuilder<Employee> builder)
    {
        builder
            .ToTable("Employees")
            .HasKey(e => e.ServiceNumber);

        builder
            .HasOne(e => e.User)
            .WithMany()
            .HasForeignKey(e => e.UserID)
            .OnDelete(DeleteBehavior.Restrict);
        
        builder
            .HasIndex(e => e.ServiceNumber)
            .IsUnique();
        builder
            .Property(e => e.Position)
            .IsRequired()
            .HasMaxLength(50);
        builder
            .Property(e => e.FirstName)
            .IsRequired()
            .HasMaxLength(30);
        builder
            .Property(e => e.SecondName)
            .IsRequired()
            .HasMaxLength(30);
        builder
            .Property(e => e.MiddleName)
            .HasMaxLength(30);
        builder
            .Property(e => e.PhoneNumber)
            .HasMaxLength(10);
    }

    public void UserConfigure(EntityTypeBuilder<User> builder)
    {
        builder
            .ToTable("Users")    
            .HasKey(u => u.ID);
        builder
            .HasIndex(u => u.Login)
            .IsUnique();
        builder
            .Property(u => u.Login)
            .IsRequired()
            .HasMaxLength(30);
        builder
            .Property(u => u.Password)
            .IsRequired()
            .HasMaxLength(256);

        builder
            .HasOne(u => u.Pass)
            .WithMany()
            .HasForeignKey(u => u.PassID)
            .OnDelete(DeleteBehavior.Cascade);
    }
}
