﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheAccessSystemCourseWork.Models;

public class PassOperation
{
    [Key]
    [DisplayName("Идентификатор операции")]
    public Guid ID { get; set; }
    [DisplayName("Тип операции")]
    public string OperationType { get; set; }
    [DisplayName("Идентификатор пропуска")]
    public Guid PassID { get; set; }
    [DisplayName("Табельный совершившего операцию")]
    public int EmployeeNumber { get; set; }
    [DisplayName("Время действия")]
    public DateTime CreatedAt { get; set; }
    [DisplayName("Пропуск")]
    public virtual Pass Pass { get; set; }
    [DisplayName("Сотрудник")]
    public virtual Employee Employee { get; set; }
}
