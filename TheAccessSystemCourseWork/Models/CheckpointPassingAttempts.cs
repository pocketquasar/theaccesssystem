﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheAccessSystemCourseWork.Models;

public class CheckpointPassingAttempts
{
    [Key]
    [DisplayName("Идентификатор попытки")]
    public Guid ID { get; set; }
    [DisplayName("Идентификатор пропуска")]
    public Guid PassID { get; set; }
    [DisplayName("Идентификатор КТ")]
    public string CheckpointID { get; set; }
    [DisplayName("Контрольная точка")]
    public virtual Checkpoint Checkpoint { get; set; }
    [DisplayName("Пропуск")]
    public virtual Pass Pass { get; set; }
    [DisplayName("Успешность попытки")]
    public bool Accessed { get; set; }
    [DisplayName("Дата и время попытки")]
    public DateTime AttemptTime { get; set; }
}