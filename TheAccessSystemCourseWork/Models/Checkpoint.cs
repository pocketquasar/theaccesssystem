﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TheAccessSystemCourseWork.Models;

public class Checkpoint
{
    [Key]
    [DisplayName("Идентификатор")]
    public string ID { get; set; }
    [DisplayName("Идентификатор помещения")]
    public int AreaID { get; set; }
    [DisplayName("Помещение")]
    public virtual FactoryArea? FactoryArea { get; set; }
    [DisplayName("Статус")]
    public bool IsActive { get; set; } = true;
    [DisplayName("Статус блокировки")]
    public bool IsBlocked { get; set; } = false;
    [DisplayName("Список прав")]
    public virtual ICollection<CheckpointRights> CheckpointRights { get; set; } = new HashSet<CheckpointRights>();
}