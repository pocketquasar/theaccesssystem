﻿namespace TheAccessSystemCourseWork.Models;

public class PersonalAccountViewModel
{
    public User User { get; set; }
    public Guest Guest { get; set; }
    public Employee Employee { get; set; }
}