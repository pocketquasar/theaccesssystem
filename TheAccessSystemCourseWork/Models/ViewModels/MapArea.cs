﻿namespace TheAccessSystemCourseWork.Models;

public class MapArea
{
    public string RoomId { get; set; }
    public string RoomName { get; set; }
    public string RoomDescription { get; set; }
    public double X { get; set; }
    public double Y { get; set; }
}

public class Floor
{
    public int FloorNumber { get; set; }
    public string FloorImage { get; set; }
    public List<MapArea> MapAreas { get; set; }
}

public class Building
{
    public string BuildingName { get; set; }
    public List<Floor> Floors { get; set; }
}