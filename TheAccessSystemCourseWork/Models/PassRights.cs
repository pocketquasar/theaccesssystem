﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TheAccessSystemCourseWork.Models;

public class PassRights
{
    [Key]
    [DisplayName("Идентификатор")]
    public int ID { get; set; }
    [DisplayName("Идентификатор пропуска")]
    public Guid PassID { get; set; }
    [DisplayName("Пропуск")]
    public virtual Pass Pass { get; set; }
    [DisplayName("Идентификатор уровня прав")]
    public string RightsLevelID { get; set; }
    [DisplayName("Уровень прав")]
    public virtual RightsLevel RightsLevel { get; set; }
}