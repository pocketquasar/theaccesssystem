﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace TheAccessSystemCourseWork.Models;

public class User
{
    [Key]
    public Guid ID { get; set; }
    [Required(ErrorMessage = "Необходимо указать роль")]
    [DisplayName("Роль")]
    public Roles Role { get; set; }
    [DisplayName("Пропуск")]
    public Guid PassID { get; set; }
    public virtual Pass? Pass { get; set; }
    [Required(ErrorMessage = "Логин обязателен")]
    [DisplayName("Логин")]
    public string Login { get; set; }

    [Required(ErrorMessage = "Пароль обязателен")]
    [DisplayName("Пароль")]
    public string Password { get; set; }
}

public enum Roles
{
    [Display(Name = "Администратор")]
    Admin = 1,
    
    [Display(Name = "Сотрудник")]
    Employee = 2,
    
    [Display(Name = "Сотрудник бюро пропусков")]
    PassOfficeEmployee = 3,
    
    [Display(Name = "Сотрудник службы безопасности")]
    SecurityServiceEmployee = 4,
    
    [Display(Name = "Гость")]
    Guest = 5
}

public static class EnumExtensions
{
    public static string GetDisplayName(this Enum enumValue)
    {
        return enumValue.GetType()
            .GetMember(enumValue.ToString())
            .First()
            .GetCustomAttribute<DisplayAttribute>()?
            .GetName() ?? enumValue.ToString();
    }
}
