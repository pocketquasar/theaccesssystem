﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheAccessSystemCourseWork.Models;


public class RightsLevel
{
    /*
     * Будет задаваться в формате "НомерПодразделения(3символа):обязательностьДолжности:Уровень прав(2 символа)"
     * По типу: 003:1:07
     *  И если проверка должности обязательна, то Должность не должна быть пустой и должна проверяться для доступа.
     */
    [Key]
    [DisplayName("Идентификатор")]
    public string ID { get; set; }
    [DisplayName("Описание")]
    public string Description { get; set; }
    [DisplayName("Необходимое подразделение")]
    public int? RequiredDepartment { get; set; }
    [DisplayName("Необходимая должность")]
    public string? RequiredPosition { get; set; }
}