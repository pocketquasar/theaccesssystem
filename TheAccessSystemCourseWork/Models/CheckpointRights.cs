﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TheAccessSystemCourseWork.Models;

public class CheckpointRights
{
    [Key]
    [DisplayName("Идентификатор")]
    public int ID { get; set; }
    [DisplayName("Идентификатор КТ")]
    public string CheckpointID { get; set; }
    [DisplayName("Контрольная точка")]
    public virtual Checkpoint Checkpoint { get; set; }
    [DisplayName("Идентификатор уровня прав")]
    public string RightsLevelID { get; set; }
    [DisplayName("Уровень прав")]
    public virtual RightsLevel RightsLevel { get; set; }
}