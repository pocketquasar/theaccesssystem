﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TheAccessSystemCourseWork.Models;

public class Pass
{
    [Key]
    public Guid ID { get; set; }
    [Required]
    [DisplayName("Дата и время создания")]
    public DateTime CreatedAt { get; set; } = DateTime.Now;
    [Required]
    [DisplayName("Тип пропуска")]
    public PassType Type { get; set; }
    public bool IsActive { get; set; } = true;
    public bool IsAttached { get; set; } = false;
    [Required]
    [DisplayName("Дата деактивации")]
    public DateTime ExpiredAt { get; set; }
    public virtual ICollection<PassRights> PassRights { get; set; } = new HashSet<PassRights>();
}

public enum PassType
{
    [Display(Name = "Разовый")]
    Guest = 1,
    
    [Display(Name = "Временный")]
    Temporary = 2,
    
    [Display(Name = "Постоянный")]
    Regular = 3
}